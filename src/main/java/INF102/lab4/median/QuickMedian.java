package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        
        int middleIndex = listCopy.size() / 2;
        int left = 0;
        int right = listCopy.size() - 1;
        
        while (true) {
            int pivotIndex = partition(listCopy, left, right);
            
            if (pivotIndex == middleIndex) {
                return listCopy.get(pivotIndex);
            } else if (pivotIndex < middleIndex) {
                left = pivotIndex + 1;
            } else {
                right = pivotIndex - 1;
            }
        }
    }
    
    private <T extends Comparable<T>> int partition(List<T> list, int left, int right) {
        T pivotValue = list.get(right);
        int storeIndex = left;
        
        for (int i = left; i < right; i++) {
            if (list.get(i).compareTo(pivotValue) < 0) {
                swap(list, i, storeIndex);
                storeIndex++;
            }
        }
        
        swap(list, storeIndex, right);
        return storeIndex;
    }
    
    private <T extends Comparable<T>> void swap(List<T> list, int i, int j) {
        T temp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, temp);
    }


}
