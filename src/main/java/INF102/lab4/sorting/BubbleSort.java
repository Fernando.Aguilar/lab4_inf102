package INF102.lab4.sorting;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
      if(list.size()<=1)
			return;
     
		//divide
		int mid = list.size()/2; 
		ArrayList<T> first = new ArrayList<>(list.subList(0, mid));
		ArrayList<T> second = new ArrayList<>(list.subList(mid, list.size())); 

		//recurse
		sort(first);
		sort(second);

		//conquer
		ArrayList<T> listToMerge = new ArrayList<T>(list); 
		merge(new LinkedList<T>(first),new LinkedList<T>(second),listToMerge); 
		list.clear(); 
		for(T t : listToMerge)
			list.add(t);
      
	}

	

    private <T extends Comparable<? super T>> void merge(LinkedList<T> first, LinkedList<T> second, ArrayList<T> list) {
		for(int i=0; i<list.size(); i++) { 
			if(second.isEmpty() || (!first.isEmpty() && first.get(0).compareTo(second.get(0))<0)) { 
				list.set(i, first.get(0)); 
				first.remove(0); 
			}
			else {
				list.set(i, second.get(0)); 
				second.remove(0); 
			}
		}
	}
    
    
}
